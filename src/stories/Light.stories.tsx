import type { Meta, StoryObj } from "@storybook/react";

import Light from "./Light";

const meta: Meta<typeof Light> = {
  component: Light,
  title: "Example/Light",
  tags: ["autodocs"],
  argTypes: {
    variant: {
      control: { type: "select" },
      options: ["green", "yellow", "red"],
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

/** This is the base color */
export const Base: Story = {
  args: { variant: "green" },
};

export const Red: Story = {
  args: { variant: "red" },
};

export const Yellow: Story = {
  args: { variant: "yellow" },
};

export const Grouped: Story = {
  render: (args) => (
    <div
      style={{
        background: "gray",
        display: "flex",
        flexDirection: "column",
        gap: 10,
        border: "2px solid black",
        width: "max-content",
        padding: 10,
      }}
    >
      <Light {...args} variant="red" />
      <Light variant="yellow" />
      <Light variant="green" />
    </div>
  ),
};
