// import React from "react";

type Props = {
  /** some description */
  variant?: "green" | "yellow" | "red";
};

/** You can customize component documentation with comments */

const Light = ({ variant = "green" }: Props) => {
  return (
    <div
      style={{
        background: variant,
        borderRadius: "50%",
        width: 50,
        height: 50,
      }}
    ></div>
  );
};
export default Light;
