import { create } from "@storybook/theming/create";

export default create({
  base: "light",
  brandTitle: "Storybook TS PL",
  brandTarget: "_self",
});
